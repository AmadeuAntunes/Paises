﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Paises
{
    public partial class Sobre : Form
    {
        Paises win; 
        public Sobre(Paises win)
        {
            InitializeComponent();
            this.win = win;
        }

        private void Sobre_FormClosed(object sender, FormClosedEventArgs e)
        {
            win.Enabled = true;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(linkLabel1.Text);
        }
    }
}
