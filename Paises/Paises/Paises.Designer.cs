﻿namespace Paises
{
    partial class Paises
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboBoxPaises = new System.Windows.Forms.ComboBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.LblCheck = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelCapital = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelRegiao = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LabelMoeda = new System.Windows.Forms.Label();
            this.LabelPopulacao = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label6 = new System.Windows.Forms.Label();
            this.LabelTime = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LabelFusoHorario = new System.Windows.Forms.Label();
            this.LabelDominio = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.PictureBox = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ComboBoxPaises
            // 
            this.ComboBoxPaises.BackColor = System.Drawing.Color.White;
            this.ComboBoxPaises.FormattingEnabled = true;
            this.ComboBoxPaises.Location = new System.Drawing.Point(45, 41);
            this.ComboBoxPaises.Name = "ComboBoxPaises";
            this.ComboBoxPaises.Size = new System.Drawing.Size(288, 21);
            this.ComboBoxPaises.TabIndex = 0;
            this.ComboBoxPaises.UseWaitCursor = true;
            this.ComboBoxPaises.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPaises_SelectedIndexChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(29, 629);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(693, 23);
            this.progressBar1.TabIndex = 1;
            this.progressBar1.UseWaitCursor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 602);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Status:";
            this.label1.UseWaitCursor = true;
            // 
            // LblCheck
            // 
            this.LblCheck.AutoSize = true;
            this.LblCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCheck.Location = new System.Drawing.Point(89, 605);
            this.LblCheck.Name = "LblCheck";
            this.LblCheck.Size = new System.Drawing.Size(153, 20);
            this.LblCheck.TabIndex = 3;
            this.LblCheck.Text = "Verificando conexão";
            this.LblCheck.UseWaitCursor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(41, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Capital:";
            this.label2.UseWaitCursor = true;
            // 
            // LabelCapital
            // 
            this.LabelCapital.AutoSize = true;
            this.LabelCapital.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.LabelCapital.Location = new System.Drawing.Point(109, 262);
            this.LabelCapital.Name = "LabelCapital";
            this.LabelCapital.Size = new System.Drawing.Size(0, 20);
            this.LabelCapital.TabIndex = 6;
            this.LabelCapital.UseWaitCursor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(41, 303);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Região:";
            this.label3.UseWaitCursor = true;
            // 
            // LabelRegiao
            // 
            this.LabelRegiao.AutoSize = true;
            this.LabelRegiao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.LabelRegiao.Location = new System.Drawing.Point(111, 303);
            this.LabelRegiao.Name = "LabelRegiao";
            this.LabelRegiao.Size = new System.Drawing.Size(0, 20);
            this.LabelRegiao.TabIndex = 10;
            this.LabelRegiao.UseWaitCursor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(359, 329);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Moeda:";
            this.label5.UseWaitCursor = true;
            // 
            // LabelMoeda
            // 
            this.LabelMoeda.AutoSize = true;
            this.LabelMoeda.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.LabelMoeda.Location = new System.Drawing.Point(421, 329);
            this.LabelMoeda.Name = "LabelMoeda";
            this.LabelMoeda.Size = new System.Drawing.Size(0, 20);
            this.LabelMoeda.TabIndex = 12;
            this.LabelMoeda.UseWaitCursor = true;
            // 
            // LabelPopulacao
            // 
            this.LabelPopulacao.AutoSize = true;
            this.LabelPopulacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.LabelPopulacao.Location = new System.Drawing.Point(135, 323);
            this.LabelPopulacao.Name = "LabelPopulacao";
            this.LabelPopulacao.Size = new System.Drawing.Size(0, 20);
            this.LabelPopulacao.TabIndex = 14;
            this.LabelPopulacao.UseWaitCursor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(41, 323);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 20);
            this.label7.TabIndex = 13;
            this.label7.Text = "População:";
            this.label7.UseWaitCursor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sobreToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(734, 24);
            this.menuStrip1.TabIndex = 15;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.UseWaitCursor = true;
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.sobreToolStripMenuItem.Text = "Sobre";
            this.sobreToolStripMenuItem.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(359, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 20);
            this.label6.TabIndex = 16;
            this.label6.Text = "Horas:";
            this.label6.UseWaitCursor = true;
            // 
            // LabelTime
            // 
            this.LabelTime.AutoSize = true;
            this.LabelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.LabelTime.Location = new System.Drawing.Point(411, 39);
            this.LabelTime.Name = "LabelTime";
            this.LabelTime.Size = new System.Drawing.Size(0, 20);
            this.LabelTime.TabIndex = 17;
            this.LabelTime.UseWaitCursor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.Location = new System.Drawing.Point(42, 343);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 20);
            this.label8.TabIndex = 18;
            this.label8.Text = "Fuso Horário:";
            this.label8.UseWaitCursor = true;
            // 
            // LabelFusoHorario
            // 
            this.LabelFusoHorario.AutoSize = true;
            this.LabelFusoHorario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.LabelFusoHorario.Location = new System.Drawing.Point(150, 343);
            this.LabelFusoHorario.Name = "LabelFusoHorario";
            this.LabelFusoHorario.Size = new System.Drawing.Size(0, 20);
            this.LabelFusoHorario.TabIndex = 19;
            this.LabelFusoHorario.UseWaitCursor = true;
            // 
            // LabelDominio
            // 
            this.LabelDominio.AutoSize = true;
            this.LabelDominio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.LabelDominio.Location = new System.Drawing.Point(194, 283);
            this.LabelDominio.Name = "LabelDominio";
            this.LabelDominio.Size = new System.Drawing.Size(0, 20);
            this.LabelDominio.TabIndex = 21;
            this.LabelDominio.UseWaitCursor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label10.Location = new System.Drawing.Point(41, 283);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(153, 20);
            this.label10.TabIndex = 20;
            this.label10.Text = "Dominio na Internet:";
            this.label10.UseWaitCursor = true;
            // 
            // PictureBox
            // 
            this.PictureBox.Location = new System.Drawing.Point(45, 68);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(288, 185);
            this.PictureBox.TabIndex = 4;
            this.PictureBox.TabStop = false;
            this.PictureBox.UseWaitCursor = true;
            // 
            // Paises
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 661);
            this.Controls.Add(this.LabelDominio);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.LabelFusoHorario);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.LabelTime);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LabelPopulacao);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.LabelMoeda);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LabelRegiao);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LabelCapital);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PictureBox);
            this.Controls.Add(this.LblCheck);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.ComboBoxPaises);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Paises";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxPaises;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblCheck;
        private System.Windows.Forms.PictureBox PictureBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelCapital;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelRegiao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LabelMoeda;
        private System.Windows.Forms.Label LabelPopulacao;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LabelTime;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LabelFusoHorario;
        private System.Windows.Forms.Label LabelDominio;
        private System.Windows.Forms.Label label10;
    }
}

