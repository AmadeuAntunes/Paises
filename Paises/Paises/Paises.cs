﻿namespace Paises
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Services;
    using System.Threading.Tasks;
    using System.Drawing;
    using System.Linq;
    using System.Threading;
    using System.Data.SQLite;
    using System.Collections;
    using System.Diagnostics;

    public partial class Paises : Form
    {
        static List<Pais> Lista = new List<Pais>();
        static List<Horarios> ListaHorarios = new List<Horarios>();

        static List<string> sinais =new List<string>();
        static List<string> horasto = new List<string>();
        static Thread thread;
        bool isInternet = false;
       
        Task tarefa;
        bool treadstart = false;
        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        
        public Paises()
        {
            InitializeComponent();
            progressBar1.Value = 0;
            myTimer.Tick += new EventHandler(TimerEventProcessor);
            isInternet = Api.IsInternet();
            DataBase.ConectarBaseDados();
           
            if (isInternet)
            {

                LblCheck.Refresh();
                StartLoadAPI();
                Lista = Api.Lista;
                DataBase.CriarTabelas();


            }
            else
            {
                Lista = DataBase.LerDados();
                LblCheck.Text = "Sem internet";
                LblCheck.ForeColor = Color.Red;
                LoadCombo();
                progressBar1.Value = 100;
            }

            myTimer.Enabled = true;
            myTimer.Start();
            myTimer.Interval = 1000;

        }



        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            LabelTime.Text = Horarios.calTime(ListaHorarios);
            if(treadstart == true)
            {
                if(tarefa.IsCompleted)
                {
                    LblCheck.Text = "Programa pronto para ser usado!";
                    progressBar1.Value = 100;
                    ComboBoxPaises.Enabled = true;
                    UseWaitCursor = false;
                }
               
            }
           
         
          
        }

        public async void StartLoadAPI()
        {
            LblCheck.ForeColor = Color.Green;
            LblCheck.Text = "Conexão à internet estabelecida (Aguardando download)";
            await LoadAPI();
            atualizarbasedados();
        }
       

        public void atualizarbasedados()
        {

            LblCheck.Text = "Atualizando a base de dados";
            ComboBoxPaises.Enabled = false;
            UseWaitCursor = true;

            foreach (var item in Lista)
            {
               
               
                tarefa = Task.Run(() =>
                {
                    treadstart = true;
                Api.DownloadImg(item.flag, item.alpha3Code);
                DataBase.InserirDados("pais",
                    "alpha2Code, nome, alpha3Code, nomeTraducao, regiao, populacao, capital",
                    "'" + item.alpha2Code + "', '" + item.name.ToString().Replace("'", "''") + "', '" + item.alpha3Code + "', '" + item.translations.pt.Replace("'", "''") + "', '" + item.region.Replace("'", "''") + "', '" + item.population + "', '" + item.capital.Replace("'", "''") + "'");

                foreach (var currence in item.currencies)
                {
                    DataBase.InserirDados("currencies",
                  "code, name, symbol",
                  "'" + currence.code + "', '" + currence.name + "', '" + currence.symbol + "'");

                    DataBase.InserirDados("moedapais",
                "code, alpha2Codes",
                "'" + currence.code + "', '" + item.alpha2Code + "'");
                }

                foreach (var domain in item.topLevelDomain)
                {
                    DataBase.InserirDados("topLevelDomain",
              "topLevelDomain, alpha2Code",
              "'" + domain + "', '" + item.alpha2Code + "'");
                }

                foreach (var time in item.timezones)
                {
                    DataBase.InserirDados("timezonesTable",
                  "timezones, alpha2Codes",
                  "'" + time + "', '" + item.alpha2Code + "'");

                       
                 }


                });

              
               

            }
          
        }


      
        private  async Task LoadAPI()
        {
            await Api.Connect();
            LoadCombo();
        }
        private void LoadCombo()
        {

            if (Lista.Capacity != 0)
            {
                Pais last = Lista.Last();
                string bandeira = String.Empty;
                foreach (var item in Lista)
                {

                    ComboBoxPaises.Items.Add(item.name);

                    if (item.Equals(last))
                    {
                        ComboBoxPaises.DisplayMember = "nome";
                        ComboBoxPaises.SelectedIndex = 0;
                        bandeira = item.alpha3Code.ToString();
                        PictureBox.Image = new Bitmap(@"..\..\img\ImgPng\" + Lista[0].alpha3Code.ToLower() + ".Bmp");

                    }
                }


            }else
            {
                LblCheck.Text = "Sem internet e base de dados local não encontrada";
            }

        }
        private void ComboBoxPaises_SelectedIndexChanged(object sender, EventArgs e)
        {
            string bandeira = String.Empty;
            IEnumerable<Pais> Qimg;
            try
            {
                Qimg = from paises in Lista
                where paises.name == ComboBoxPaises.SelectedItem.ToString()
                select paises;
            }
            catch (Exception)
            {

                throw;
            }

            ListaHorarios.Clear();

            foreach (var pais in Qimg)
            {
                bandeira = pais.alpha3Code.ToString();
                LabelCapital.Text = pais.capital;
                LabelRegiao.Text = pais.region;
                LabelPopulacao.Text = pais.population.ToString("#,##0");  
                string dominio = String.Empty;
                foreach (var item in pais.topLevelDomain)
                {
                    dominio += item.ToString() + " ";
                }
                LabelDominio.Text = dominio;

                string fusoHorario = String.Empty;
                ListaHorarios.Clear(); 
                foreach (var item in pais.timezones)
                {
                        ListaHorarios.Add(Horarios.timezone(item.ToString()));
                        fusoHorario += item.ToString() + " [" + item.ToString() + "]" + "\n";
                }
                LabelFusoHorario.Text = fusoHorario;

                string moeda = String.Empty;
                foreach (var item in pais.currencies)
                {
                    try
                    {
                        moeda += item.symbol.ToString() + " [" + item.name.ToString() + "]" + "\n";
                    }
                    catch(Exception)
                    {
                     
                    }
                   
                }
                LabelMoeda.Text = moeda;
            }
          
            PictureBox.ImageLocation = Api.GetImg(bandeira);


        }
        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            Sobre win = new Sobre(this);
            win.StartPosition =  FormStartPosition.CenterScreen;
            win.Show();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (thread != null)
               thread.Abort();
               DataBase.close();
        }
    }
}
