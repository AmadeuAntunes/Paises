﻿namespace Paises.Services
{
    using Modelos;
    using Newtonsoft.Json;
    using Svg;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Drawing.Imaging;
    using System.Net;
    using System.Net.Http;
    using System.Net.NetworkInformation;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public class Api
    {
        public static List<Pais> Lista = new List<Pais>();
        public  static bool isInternet = false;
        public  static async Task Connect()
        {
            if(IsInternet())
            {

            
                var cliente = new HttpClient();
                cliente.BaseAddress = new Uri("https://restcountries.eu");
                HttpResponseMessage response = await cliente.GetAsync("rest/v2/all");
                string result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return;
                }
                List<Pais> L = JsonConvert.DeserializeObject<List<Pais>>(result);
                foreach (var item in L)
                {
                    if (item != null)
                    {
                        Lista.Add(item);
                    }
                }
            }

        }

        public static bool DownloadImg(string urll, string name)
        {


            string url = urll;
            using (WebClient client = new WebClient())
            {
                client.DownloadFileAsync(new Uri(url), @"..\..\img\ImgSvg\" + name.ToLower() + ".svg");
            }
            return true;
        }
        public static bool IsInternet()
        {
            try
            {
                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                if (reply.Status == IPStatus.Success)
                {
                    isInternet = true;
                }
                return isInternet;
            }
            catch (Exception)
            {
                MessageBox.Show(isInternet.ToString());
                return isInternet;
            }
        
        }

        public static string GetImg(string bandeira)
        {

            String path = @"..\..\img\ImgSvg\" + bandeira.ToLower() + ".svg";
            String pngPath = @"..\..\img\ImgPng\" + bandeira.ToLower() + ".Bmp";
            try
            {
                    var svgDocument = SvgDocument.Open(path);
                using (var smallBitmap = svgDocument.Draw())
                {
                    var width = smallBitmap.Width;
                    var height = smallBitmap.Height;
                    if (width != 288 || height != 185)
                    {
                        width = 288;
                        height = 185;
                    }

                    using (var bitmap = svgDocument.Draw(width, height))
                    {

                        bitmap.Save(pngPath, ImageFormat.Bmp);
                        return pngPath;
                    }
                }
            }
            catch (Exception)
            {
                return "";
            }
        }


     


    }
}
