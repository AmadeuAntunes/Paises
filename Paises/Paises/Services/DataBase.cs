﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Windows.Forms;
using Paises.Modelos;

namespace Paises.Services
{
    public  class DataBase
    {
        static  SQLiteConnection m_dbConnection;
        static string NomeBaseDados { get; set; }

        public  DataBase(string nomeBaseDados)
        {
            NomeBaseDados = nomeBaseDados;
            try
            {
                SQLiteConnection.CreateFile("../../sqLite/" + NomeBaseDados + ".sqlite");
            }
            catch (Exception e)
            {
               MessageBox.Show(e.Message);
              
            }
           



        }


        public static void ConectarBaseDados()
        {
            try
            {
                 m_dbConnection = new SQLiteConnection("Data Source=../../sqLite/" + NomeBaseDados + ".sqlite;Version=3;");
                 m_dbConnection.Open();
            }
            catch (SQLiteException e)
            {
               MessageBox.Show(e.Message);
            }
        }

        public static void CriarTabelas()
        {
            try
            {
                string sql = "CREATE TABLE IF NOT EXISTS `Pais` ( `alpha2Code` TEXT NOT NULL, `nome` TEXT, `alpha3Code` TEXT,`nomeTraducao` TEXT, `regiao` TEXT, `populacao` TEXT, `capital` TEXT, PRIMARY KEY(`alpha2Code`) )";
                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();

                sql = "CREATE TABLE IF NOT EXISTS `currencies` ( `code` TEXT NOT NULL, `name` TEXT, `symbol` TEXT, PRIMARY KEY(`code`) )";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();

                sql = "CREATE TABLE IF NOT EXISTS `timezonesTable` ( `timezones` TEXT, `alpha2Codes` TEXT)";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();

                sql = "CREATE TABLE IF NOT EXISTS `topLevelDomain` ( `topLevelDomain` TEXT  NOT NULL, `alpha2Code` TEXT , PRIMARY KEY(`topLevelDomain`) )";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();

                sql = "CREATE TABLE IF NOT EXISTS `moedapais` ( `code` TEXT NOT NULL,  `alpha2Codes` TEXT, PRIMARY KEY(`code`, `alpha2Codes` ) )";
                command = new SQLiteCommand(sql, m_dbConnection);
                command.ExecuteNonQuery();

             
            }
            catch (SQLiteException)
            {

               
            }
           
        }

   

        public static void InserirDados(string nomeTabela, string nomeColuna, string dados)
        {

            string sql = "INSERT  OR IGNORE INTO " + nomeTabela + " ("+ nomeColuna + ")" + 
                         " VALUES ("+ dados +")";
           
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (SQLiteException g)
            {
              // MessageBox.Show(g.Message);
            }

        }
        public static void close()
        {
            m_dbConnection.Close();
        }

        public static List<Pais> LerDados()
        {
            List<Pais> lista = new List<Pais>();
            try
            {
          
            SQLiteCommand sqCommand = (SQLiteCommand)m_dbConnection.CreateCommand();
            sqCommand.CommandText = "SELECT * FROM pais";

            using (SQLiteDataReader rdr = sqCommand.ExecuteReader())
            {
                while (rdr.Read())
                {
                    Pais pais = new Pais();
                    pais.alpha2Code = rdr["alpha2Code"].ToString();
                    pais.alpha3Code = rdr["alpha3Code"].ToString();
                    pais.name = rdr["nome"].ToString();
                    pais.region = rdr["regiao"].ToString();
                    pais.population = Convert.ToInt32(rdr["Populacao"]);
                    pais.capital = rdr["capital"].ToString();
                    pais.timezones = new List<string>();

                    List<string> topLevelDomainlista = new List<string>();
                    SQLiteCommand sqCommand1 = (SQLiteCommand)m_dbConnection.CreateCommand();
                    sqCommand1.CommandText = "SELECT * FROM topLevelDomain where alpha2Code = '" + rdr["alpha2Code"].ToString() + "'";
                    using (SQLiteDataReader rdr1 = sqCommand1.ExecuteReader())
                    {
                        while (rdr1.Read())
                        {
                            topLevelDomainlista.Add(rdr1["topLevelDomain"].ToString());
                        }
                    }
                    pais.topLevelDomain = topLevelDomainlista;

                    List<Currency> currencieslista = new List<Currency>();
                    sqCommand1.CommandText = "SELECT Currencies.code, Currencies.name, Currencies.symbol " +
                    "FROM moedapais " +
                    "INNER JOIN Currencies ON (Currencies.code = moedapais.code) " +
                    "INNER JOIN Pais ON(Pais.alpha2Code = moedapais.alpha2Codes) " +
                    "WHERE alpha2Codes = '" + rdr["alpha2Code"].ToString() + "'";
                
                    using (SQLiteDataReader rdr1 = sqCommand1.ExecuteReader())
                    {
                        while (rdr1.Read())
                        {
                            Currency c = new Currency();
                            c.code = rdr1["code"].ToString();
                            c.name = rdr1["name"].ToString();
                            c.symbol = rdr1["symbol"].ToString();
                            currencieslista.Add(c);
                        }
                    }
                    pais.currencies = currencieslista;

                    List<string> time = new List<string>();
                    sqCommand1.CommandText = "SELECT timezones from timezonesTable where alpha2Codes = '" + rdr["alpha2Code"].ToString() + "'";
                    using (SQLiteDataReader rdr1 = sqCommand1.ExecuteReader())
                    {
                        while (rdr1.Read())
                        {
                           if(!pais.timezones.Contains(rdr1["timezones"].ToString()))
                            pais.timezones.Add(rdr1["timezones"].ToString());
                        }
                    }
                   
                    lista.Add(pais);
                }
            }
               
 SQLiteDataReader sqReader = sqCommand.ExecuteReader();
            }
            catch (SQLiteException g)
            {
              //  MessageBox.Show(g.Message);
                
            }
            return lista;

        }

       
    }
}
