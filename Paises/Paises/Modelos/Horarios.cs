﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Paises.Modelos
{
    public class Horarios
    {
        public string sinal;
        public string horas;
        public string minutos;
        public Horarios(string sinal, string horas, string minutos)
        {
            this.horas = horas;
            this.minutos = minutos;
            this.sinal = sinal;
        }


       public static Horarios timezone(string t)
        {
            string s = string.Empty;
            string horas = string.Empty;
            string minutos = string.Empty;
            if (t.Length > 7)
            {
                s = t.Substring(3, 1);
                horas = t.Substring(4, 2);
                minutos = t.Substring(7, 2);

            }
            else
            {
                s = "+";
                horas = "0";
                minutos = "0";
            }
            return (new Horarios(s, horas, minutos));

        }

        public static string calTime(List<Horarios> ListaHorarios)
        {
            List<DateTime> H = new List<DateTime>();
            int M = Convert.ToInt32(DateTime.Now.Minute);
            int S = Convert.ToInt32(DateTime.Now.Second);
            DateTime Htemp = DateTime.Now;
           

            string tcom = String.Empty;

            try
            {

                foreach (var horarios in ListaHorarios)
                {
                   
                    switch (horarios.sinal)
                    {
                        case "+":
                            {
                                Htemp = DateTime.Now.AddHours(Convert.ToDouble(horarios.horas));
                                H.Add(Htemp.AddMinutes(Convert.ToDouble(horarios.minutos)));
                                break;
                            }
                        case "-":
                            {
                                H.Add(DateTime.Now.Subtract(new TimeSpan(0, Convert.ToInt32(horarios.horas), Convert.ToInt32(horarios.minutos), 0)));
                                break;
                            }

                    }
                
                



                }
                tcom = String.Empty;
                foreach (var item in H)
                {
                   
                    tcom += item.Hour.ToString().PadLeft(2, '0') + ":" + item.Minute.ToString().PadLeft(2, '0') + ":" + item.Second.ToString().PadLeft(2, '0') + "\n";

                }

            }

            catch (Exception)
            {


            }
            return tcom;
        }



    }
}
